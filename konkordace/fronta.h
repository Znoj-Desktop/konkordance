/*
 * Soubor:  fronta.h
 * Datum:   2012/04/28
 * Autor:   Jiri Znoj, jiri.znoj.st@vsb.cz
 * Projekt: Tvorba konkordance
 * Popis:   knihovny, deklarace promennych a prototypy funkci fronty
 */

#ifndef _FRONTA_H_
#define _FRONTA_H_

#include <fstream>
#include <iostream>
using namespace std;

class queue{
	private:
		int key; //cislo radku na kterym se slovo vyskytuje
		queue* next; //ukazatel na dalsi prvek fronty
		queue* teil; //ukazatel na posledni (posledni pridany) prvek fronty

	public:
		queue(int key); //konstruktor fronty
		void insertKey(int key); //vlozeni klice do fronty
		int getLastKey(void); //funkce vraci posledni vlozeny klic do fronty
		void printQueue(ofstream *vystup); //vytisknuti klicu fronty vzestupne
		void cancelQueue(void); //dealokace naalokovanych prvku fronty
};
#endif