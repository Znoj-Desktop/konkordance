#include "bs.h" //hlavickovy soubor se vsemi potrebnymi knihovnami a konstantami

//konstruktor binarniho stromu
node::node(string text, int key) : row(key){
	this->text = text;
	this->left = NULL;
	this->right = NULL;
}

//vlozeni slova a radku do stromu
void node::insert(string text, int key){
	//kdyz pri zalozeni stromu neni definovan klic, pak prvni vlozeny klic je koren
	if(this->text.empty()){
		this->text = text;
		row.insertKey(key);
	}

	else{
		node* pom = this; //pomocna promenna pro pruchod stromem
		bool end = 0; //priznak konce cyklu

		//cyklus bezi dokud se nevytvori novy uzel nebo dokud
		//se nezapise cislo radku k jiz existujicimu uzlu
		while(end != 1){
			if(text.compare(pom->text) > 0){ //slovo je syntakticky vetsi nez prochazene
				if(pom->right == NULL){ //pokud vpravo uzel neni, tak se zde vytvori
					pom->right = new node(text, key); //alokace a pripojeni noveho uzlu jako praveho potomka
					end = 1;
				}
				else{
					pom = pom->right; //presun vpravo
				}
			}

			else if(text.compare(pom->text) < 0){ //slovo je syntakticky vetsi nez prochazene
				if(pom->left == NULL){ //pokud vlevo uzel neni, tak se zde vytvori
					pom->left = new node(text, key); //alokace a pripojeni noveho uzlu jako leveho potomka
					end = 1;
				}
				else{
					pom = pom->left; //presun vlevo
				}
			}

			else{ //zapisovane slovo se shoduje s prave prochazenym, zapise se jen cislo radku
				//cislo radku se zapise pouze pokud je to prvni vyskyt slova na tomto radku
				if(pom->row.getLastKey() != key){
					pom->row.insertKey(key);
				}
				end = 1;
			}
		} // konec while(end != 1)
	}
}

//rekurzivni vypis binarniho stromu inorder pruchodem aby byla slova serazena podle abecedy vzestupne
void node::printBT(ofstream *outFile){
	if(left != NULL){
		left->printBT(outFile); //posun vlevo
	}
	
	if(!text.empty()){ //tato podminka je zde proto, aby se nevypsalo nic kdyz je zadan soubor neobsahujici zadne pismeno
		*outFile << text << ": ";
		row.printQueue(outFile); //vypis vsech radku vyskytu daneho slova z fronty
	}

	if(right != NULL){
		right->printBT(outFile); //posun vpravo
	}
}

//rekurzivni ruseni stromu postorder pruchodem aby se koren zrusil az jako posledni
void node::cancelBT(void){
	if(left != NULL){
		left->cancelBT(); //presun vlevo
		delete this->left; //zruseni leveho potomka
		this->left = NULL;
	}

	if(right != NULL){
		right->cancelBT(); //presun vpravo
		delete this->right; //zruseni praveho potomka
		this->right = NULL;
	}

	row.cancelQueue(); //zruseni vsech naalokovanych radku s vyskyty daneho slova z fronty
	this->text.clear(); //ve stromu zustane jen koren s cislem radku 0, retezec nastavime na delku 0
}