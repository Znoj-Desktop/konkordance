#ifndef _BS_REKURZE_H_
#define _BS_REKURZE_H_

#include <string> //pro praci se stringy
#include <iostream> //pro vstupvystupni operace
#include "fronta.h" //pripojeni fronty
using namespace std;

class node{
	private:
		string text;
		queue row;
		node* left;
		node* right;

	public:
		node(string text, int key);
		void insert(string text, int key);
		void printBT(ofstream *outFile);
		void cancelBT(void);
};
#endif