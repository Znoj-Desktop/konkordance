#include "fronta.h" //hlavickovy soubor se vsemi potrebnymi knihovnami a konstantami

//konstruktor fronty
queue::queue(int key){
	this->key = key;
	this->next = NULL;
	this->teil = this;
}

//vlozeni klice do fronty
void queue::insertKey(int key){
	if(this->key == -1){//pokud se zapisuje prvni prvek, neni treba nic alokovat
		this->key = key;
	}
	else{
		//novy prvek fronty se navaze na ocas (posledni pridany prvek)
		this->teil->next = new queue(key);
		//novy prvek se stava ocasem - tedy poslednim pridanym prvkem
		this->teil = this->teil->next;
	}
}

//funkce vraci posledni vlozeny klic
int queue::getLastKey(void){
	return this->teil->key;
}

//vytisknuti klicu fronty vzestupne
void queue::printQueue(ofstream *vystup){
	//zde se ocas pouzije jako pomocny prvek, ktery ukazuje postupne na
	//vsechny prvky od hlavy
	this->teil = this;
	while(this->teil->next != NULL){
		*vystup << this->teil->key << ", ";
		this->teil = this->teil->next;
	}	
	//posledn� ciso radku konci odratkovanim a ne ", "
	*vystup << this->teil->key << endl;
}

//dealokace naalokovanych prvku fronty
void queue::cancelQueue(){
	//prvky se rusi od druheho po posledni (posledni pridany), 
	//ocas zde plni ulohu pomocneho prvku - nikoli posledniho
	while(this->next != NULL){
		this->teil = this->next;
		this->next = this->next->next;
		delete this->teil;
	}	
	//vynulovani prvniho prvku (prvniho pridaneho)
	this->key = NULL;
	this->next = NULL;
	this->teil = NULL;
}