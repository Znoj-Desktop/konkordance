#include "bs_rekurze.h"

node::node(string text, int key) : row(key){
	this->text = text;
	this->left = NULL;
	this->right = NULL;
}

void node::insert(string text, int key){
	//kdyz pri zalozeni stromu neni definovan klic, pak prvni vlozeny klic je koren
	
	if(this->text.empty()){
		this->text = text;
	}

	else if(text.compare(this->text) > 0){
		if(this->right == NULL){
			this->right = new node(text, key);
		}
		else{
			this->right->insert(text, key);
		}
	}

	else if(text.compare(this->text) < 0){
		if(this->left == NULL){
			this->left = new node(text, key);
		}
		else{
			this->left->insert(text, key);
		}
	}

	else { //strcmp(string, this->string) == 0, takze se pouze pripise cislo radku
		//cislo radku se zapise pouze pokud je to prvni vyskyt slova na tomto radku
		if(row.getLastKey() != key){
			row.insertKey(key);
		}
	}

}

//vypis binarniho stromu inorder pruchodem aby byla slova serazena podle abecedy vzestupne
void node::printBT(ofstream *outFile){
	if(left != NULL){
		left->printBT(outFile);
	}
	
	if(!text.empty()){ //tato podminka je zde proto, aby se nevypsalo nic kdyz je zadan soubor neobsahujici zadne pismeno
		*outFile << text << ": ";
		row.printQueue(outFile); //vypis vsech radku vyskytu daneho slova z fronty
	}

	if(right != NULL){
		right->printBT(outFile);
	}
}

//ruseni stromu postorder pruchodem aby se koren zrusil az jako posledni
void node::cancelBT(void){
	if(left != NULL){
		left->cancelBT();
		delete this->left;
		this->left = NULL;
	}

	if(right != NULL){
		right->cancelBT();
		delete this->right;
		this->right = NULL;
	}

	row.cancelQueue(); //vypis vsech radku vyskytu daneho slova z fronty
	this->text.clear(); //ve stromu zustane jen koren s cislem radku 0, retezec nastavime na delku 0
}