/*
 * Soubor:  bs.h
 * Datum:   2012/04/28
 * Autor:   Jiri Znoj, jiri.znoj.st@vsb.cz
 * Projekt: Tvorba konkordance
 * Popis:   knihovny, deklarace promennych a prototypy funkci binarniho stromu (BS)
 */

#ifndef _BS_H_
#define _BS_H_

#include <string> //pro praci se stringy
#include <iostream> //pro vstupvystupni operace
#include "fronta.h" //pripojeni fronty
using namespace std;

class node{
	private:
		string text; //slovo ulozene do stromu
		queue row; //fronta s radky na kterych se slovo vyskytuje
		node* left; //ukazatel na leveho potomka
		node* right; //ukazatel na praveho potomka

	public:
		node(string text, int key); //konstruktor BS
		void insert(string text, int key); //vlozeni slov a radku do BS
		void printBT(ofstream *outFile); //vytisknuti serazeneho obsahu BS do souboru
		void cancelBT(void); //zruseni BS
};
#endif