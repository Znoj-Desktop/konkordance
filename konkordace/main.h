/*
 * Soubor:  main.h
 * Datum:   2012/04/28
 * Autor:   Jiri Znoj, jiri.znoj.st@vsb.cz
 * Projekt: Tvorba konkordance
 * Popis:   knihovny, hlavickove soubory a konstanty pro soubor main.c
 */

#include <string> //pro praci s retezi
#include <fstream> //pro cteni/zapis ze/do souboru
#include <iostream>
#include "ctype.h" //pro prevod na mala pismena + kontrola jestli se jedna o pismeno
#include "bs.h" //pro binarni strom
//#include "bs_rekurze.h" //pro binarni strom rekurzivne zapsany
using namespace std; //jmeny prostor je potreba pro praci se stringy

const int fileLength = 100; // maximalni delka jmena vstupniho/vystupniho souboru i s cestou
const int wordLength = 50; // maximalni delka precteneho slova

//funkce cte soubor po znacich a uklada do stromu jednotliva slova s radky na kterych se vyskytuji
void readFile(ifstream *inFile, node *tree);