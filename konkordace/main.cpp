/*
 * Soubor:  main.cpp
 * Datum:   2012/04/28
 * Autor:   Jiri Znoj, jiri.znoj.st@vsb.cz
 * Projekt: Tvorba konkordance
 * Popis:   Pro zadan� textovy soubor program sestavi seznam slov a ke kazdemu
 *			slovu vyppise seznam radku na kterych se toto slovo vyskytuje.
 *			Slova jsou v konkordanci setridena podle abecedy, radky s vyskyty
 *			jsou uvadeny vzestupne.	Vstupni text cte z textoveho souboru, 
 *			vyslednou konkordanci vypise opet do textoveho souboru.
 *			program vraci 0 pri uspechu
 *			program vraci 1 pri chybe pri otevreni vstupniho souboru
 *			program vraci 2 pri chybe pri otevreni vystupniho souboru
 *			
 */

#include "main.h" //hlavickovy soubor se vsemi potrebnymi knihovnami a konstantami

//funkce readfile cte soubor po znacich a uklada do stromu uklada jednotliva slova s radky na kterych se vyskytuji
void readFile(ifstream *inFile, node *tree){

	char word[wordLength];
	int character, counter = 0, row = 0;
	string text;

	do{ //cyklus projede cely otevreny soubor

		do{ //cyklus projede cely radek
			character = (*inFile).get();

			while(isalpha(character)){ //kazde pismeno se prevede na male, ulozi do word[] dokud neprijde jiny znak
				word[counter++] = tolower((char) character);
				character = (*inFile).get();
			}
			
			word[counter] = '\0'; //zakonceni slova aby ho vzal string a aby se vedelo kde slovo konci
			text = word; //ulozeni pole charu do stringu

			if(!text.empty()){ //pokud je slovo neprazdne, tak se ulozi do stromu spolu s aktualnim radkem
				(*tree).insert(text, row);
			}

			counter = 0; //pocitadlo pismen ve word[] nastaveno tak, aby se mohlo slovo prepsat novym
		}while(((char)character != '\n')&&(character != EOF)); //konec radku, character != EOL je zde proti zacykleni

		row++;

	}while(character != EOF);
}




int main(){
	
	char fileName[fileLength]; //promenna pro ulozeni jmena souboru i s cestou
	

	//---------------------------------------------------INPUT------------------------------------------------------------//

	cout << "Zadej nazev vstupniho souboru: ";
	cin >> fileName;

	ifstream inFile(fileName);
	if(!inFile.is_open()){ //pokud se soubor nepodari otevrit, tak program vraci chybovy kod a konci;
		cout << "soubor se nepodarilo otevrit" << endl;
		return 1;
	}
	
	node tree("", -1); //vytvoreni stromu, s klicem -1, coz znaci ze se ma koren prepsat skutecnou hodnotou

	readFile(&inFile, &tree); //volani funkce pro precteni souboru a ulozeni do stromu

	inFile.close(); 
	

	//---------------------------------------------------OUTPUT------------------------------------------------------------//


	cout << "Zadej nazev vystupniho souboru: ";
	cin >> fileName;

	ofstream outFile(fileName);
	if(!outFile.is_open()){ //pokud se soubor nepodari otevrit, tak program uvolni pamet, vraci chybovy kod a konci;
		cout << "soubor se nepodarilo otevrit" << endl;
		tree.cancelBT(); //je treba pred zavreni dealokovat naalokovanou pamet
		return 2;
	}

	tree.printBT(&outFile); //vypis v pozadovanem tvaru do vystupniho souboru

	outFile.close();


	//----------------------------------------------------------------------------------------------------------------------//

	tree.cancelBT(); //dealokace listu BS
	return 0;
}